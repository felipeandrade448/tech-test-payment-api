using Application.Common.Validation;
using Application.Sales.EventHandlers.Requests.CreateSale;
using FluentValidation;
using Shared.Exceptions;

namespace Application.Sales.EventHandlers.Validators;

public class CreateItemToSaleRequestValidator : CustomValidator<CreateItemToSaleRequest>
{
    public CreateItemToSaleRequestValidator()
    {
        RuleFor(sale => sale.Name)
            .Cascade(CascadeMode.Stop)
            .NotNull()
            .WithMessage(ErrorsMessages.Validate.ValidateSale.ValidateItem.NameRequired)
            .NotEmpty()
            .WithMessage(ErrorsMessages.Validate.ValidateSale.ValidateItem.NameRequired);

        RuleFor(sale => sale.Price)
            .Cascade(CascadeMode.Stop)
            .NotNull()
            .WithMessage(ErrorsMessages.Validate.ValidateSale.ValidateItem.PriceRequired);

        RuleFor(sale => sale.Quantity)
            .Cascade(CascadeMode.Stop)
            .NotNull()
            .WithMessage(ErrorsMessages.Validate.ValidateSale.ValidateItem.QuantityRequired);
    }
}