﻿namespace Domain.Entities;

public class Seller : AuditableEntity, IAggregateRoot
{
    public string? Document { get; set; }
    public string? Name { get; set; }
    public string? Email { get; set; }
    public string? Phone { get; set; }

}