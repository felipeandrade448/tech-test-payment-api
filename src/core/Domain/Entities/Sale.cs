﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using Domain.Enums;
using Shared.Exceptions;

namespace Domain.Entities;

[Table("Sales")]
public class Sale : AuditableEntity, IAggregateRoot
{
    public string? Code { get; set; }
    public DateTime? Date { get; set; } = DateTime.Now;
    public StatusSaleEnum? Status { get; set; } = StatusSaleEnum.AwaitingPayment;
    public Seller? Seller { get; set; }
    public List<ItemSale>? Items { get; set; }

    public Sale SetCode()
    {
        Code = $"{Date:yyyyMMdd}-{Random.Shared.Next(0, 500)}";
        return this;
    }

    public Sale UpdateStatus(StatusSaleEnum status)
    {
        switch (status)
        {
            case StatusSaleEnum.PaymentApproved when Status == StatusSaleEnum.AwaitingPayment:
                Status = status;
                break;
            case StatusSaleEnum.Canceled when Status == StatusSaleEnum.AwaitingPayment:
                Status = status;
                break;
            case StatusSaleEnum.SentToCarrier when Status == StatusSaleEnum.PaymentApproved:
                Status = status;
                break;
            case StatusSaleEnum.Canceled when Status == StatusSaleEnum.PaymentApproved:
                Status = status;
                break;
            case StatusSaleEnum.Delivered when Status == StatusSaleEnum.SentToCarrier:
                Status = status;
                break;
            default:
                throw new CustomException(string.Format(ErrorsMessages.Sale.UnableToUpdateSaleStatus, Status, status), statusCode: HttpStatusCode.UnprocessableEntity);
        }
        return this;
    }
}