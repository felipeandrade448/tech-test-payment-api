using MediatR;

namespace Application.Sales.EventHandlers.Requests.CreateSale;

public class CreateSaleRequest : IRequest<Guid?>
{
    public DateTime? Date { get; set; }
    public CreateSellerToSaleRequest Seller { get; set; } = default!;
    public List<CreateItemToSaleRequest> Items { get; set; } = default!;
}

public class CreateSellerToSaleRequest
{
    public string? Document { get; set; }
    public string? Name { get; set; }
    public string? Email { get; set; }
    public string? Phone { get; set; }
}

public class CreateItemToSaleRequest
{
    public string? Name { get; set; }
    public decimal? Price { get; set; }
    public int? Quantity { get; set; }
}