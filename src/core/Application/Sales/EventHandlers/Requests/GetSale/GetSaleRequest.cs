﻿using Application.Sales.EventHandlers.Dtos;
using MediatR;

namespace Application.Sales.EventHandlers.Requests.GetSale;

public class GetSaleRequest : IRequest<SaleResponse>
{
    public GetSaleRequest(Guid id)
    {
        Id = id;
    }

    public Guid Id { get; set; }
}