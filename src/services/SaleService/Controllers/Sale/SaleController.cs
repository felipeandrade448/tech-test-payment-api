﻿using Application.Common.Controllers;
using Application.Sales.EventHandlers.Dtos;
using Application.Sales.EventHandlers.Requests.CreateSale;
using Application.Sales.EventHandlers.Requests.GetSale;
using Application.Sales.EventHandlers.Requests.UpdateSale;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace SaleService.Controllers.Sale;

public class SaleController : VersionNeutralApiController
{
    [HttpPost]
    //[OpenApiOperation("Criar uma nova venda.", "Realiza a criação de uma nova venda.")]
    [ProducesResponseType(typeof(BaseResult<Guid>), (int)HttpStatusCode.Created)]
    [ProducesResponseType(typeof(BaseResult), (int)HttpStatusCode.BadRequest)]
    public Task<ActionResult<BaseResult<Guid>>> CreateSale(CreateSaleRequest request) =>
        Result<CreateSaleRequest, Guid>(request, result => Result(HttpStatusCode.Created, result));

    [HttpGet("{id}")]
    [ProducesResponseType(typeof(BaseResult<SaleResponse>), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(BaseResult), (int)HttpStatusCode.BadRequest)]
    //[OpenApiOperation("Obter uma venda por um identificador", "Realiza a busca de uma venda pelo identificador único do sistema.")]
    public Task<ActionResult<BaseResult<SaleResponse>>> GetSale([FromRoute] Guid id) =>
        Result<GetSaleRequest, SaleResponse>(new GetSaleRequest(id));

    /// <summary>
    /// Atualizar a venda
    /// Realiza a atualização do status da venda
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPatch("{id}/status")]
    [ProducesResponseType(typeof(BaseResult<Guid>), (int)HttpStatusCode.OK)]
    [ProducesResponseType(typeof(BaseResult), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(BaseResult), (int)HttpStatusCode.UnprocessableEntity)]
    public Task<ActionResult<BaseResult<Guid>>> UpdateSale([FromRoute] Guid id, [FromBody] UpdateSaleRequest request)
    {
        request.Id = id;
        return Result<UpdateSaleRequest, Guid>(request);
    }
}