using Application.Common.Persistence;
using Domain.Entities;
using MediatR;
using Shared.Exceptions;

namespace Application.Sales.EventHandlers.Requests.UpdateSale;

public class UpdateSaleRequestHandler : IRequestHandler<UpdateSaleRequest, Guid?>
{
    private IRepository<Sale> _repository;

    public UpdateSaleRequestHandler(IRepository<Sale> repository)
    {
        _repository = repository;
    }

    public async Task<Guid?> Handle(UpdateSaleRequest request, CancellationToken cancellationToken)
    {
        var sale = await _repository.GetByIdAsync(request.Id, cancellationToken);

        _ = sale ?? throw new CustomException(ErrorsMessages.Sale.NotFound);

        var updatedSale = sale.UpdateStatus(request.Status);

        await _repository.UpdateAsync(updatedSale, cancellationToken);

        return request.Id;
    }
}