using Application.Common.Validation;
using Application.Sales.EventHandlers.Requests.UpdateSale;
using FluentValidation;

namespace Application.Sales.EventHandlers.Validators;

public class UpdateSaleRequestValidator : CustomValidator<UpdateSaleRequest>
{
    public UpdateSaleRequestValidator()
    {
        
        RuleFor(p => p.Id)
            .NotEmpty()
            .WithMessage("O ID � de preenchimento obrigat�rio.");

        RuleFor(p => p.Status)
            .NotEmpty()
            .WithMessage("O status � de preenchimento obrigat�rio.");
    }
}