using Application.Common.Persistence;
using Domain.Entities;
using Mapster;
using MediatR;

namespace Application.Sales.EventHandlers.Requests.CreateSale;

public class CreateSaleRequestHandler : IRequestHandler<CreateSaleRequest, Guid?>
{
    private readonly IRepository<Sale> _repository;

    public CreateSaleRequestHandler(IRepository<Sale> repository)
    {
        (_repository) = (repository);
    }

    public async Task<Guid?> Handle(CreateSaleRequest request, CancellationToken cancellationToken)
    {
        var sale = request.Adapt<Sale>();
        sale = sale.SetCode();
        await _repository.AddAsync(sale, cancellationToken);
        return sale.Id;
    }
}