namespace Infrastructure.Cors;

public class CorsSettings
{
    public string? Blazor { get; set; }
    public string? Apisix { get; set; }
    public string? Keycloak { get; set; }
    public string? Apis { get; set; }
}