﻿using Microsoft.AspNetCore.Mvc;

namespace SaleService.Controllers;

[Route("api/[controller]")]
[ApiVersionNeutral]
public class VersionNeutralApiController : BaseApiController
{
}