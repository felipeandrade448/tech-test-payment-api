﻿using System.Net;
using Application.Common.Persistence;
using Application.Sales.EventHandlers.Dtos;
using Domain.Entities;
using Mapster;
using MediatR;
using Shared.Exceptions;

namespace Application.Sales.EventHandlers.Requests.GetSale;


public class GetSaleRequestHandler : IRequestHandler<GetSaleRequest, SaleResponse>
{
    private readonly IRepository<Sale> _repository;

    public GetSaleRequestHandler(IRepository<Sale> repository) =>
        (_repository) = (repository);

    public async Task<SaleResponse> Handle(GetSaleRequest request, CancellationToken cancellationToken)
    {
        var sale = await _repository.GetByIdAsync(request.Id, cancellationToken);
        return sale == null
            ? throw new CustomException(ErrorsMessages.Sale.NotFound, statusCode: HttpStatusCode.NotFound)
            : sale.Adapt<SaleResponse>();
    }
}