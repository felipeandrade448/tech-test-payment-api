workspace {

    model {
        user = person "User" "Pessoa que utiliza a API para gerenciar as vendas."

        softwareSystem = softwareSystem "Sistema de Vemdas API" "API para gerenciar as vendas de produtos." {
            apiContainer = container "API REST (.NET Core)" "Exponha operações REST para gerenciar vendas." "C# .NET Core" {
                user -> this "Utiliza"

                salesController = component "SalesController" "Controlador para operações de vendas." "C#"
                createSaleRequest = component "CreateSaleRequest" "Request a ser resolvida a lógica de negócios para criação de uma venda." "C#"
                getSaleRequest = component "GetSaleRequest" "Request a ser resolvida a lógica de negócios para a busca de uma venda por identificador." "C#"
                updateSaleRequest = component "UpdateSaleRequest" "Request a ser resolvida a lógica de negócios de atualização do status de uma venda." "C#"
                repository = component "Repository" "Repositório para persistência das vendas." "C#"
                inMemoryDatabase = component "InMemoryDatabase" "Banco de dados em memória para armazenar as vendas (Armazena no arquivo Sales.json)." "In-Memory Database"

                salesController -> createSaleRequest "Chama o request"
                salesController -> getSaleRequest "Chama o request"
                salesController -> updateSaleRequest "Chama o request"
                createSaleRequest -> repository "Chama o request"
                getSaleRequest -> repository "Chama o request"
                updateSaleRequest -> repository "Chama o request"
                repository -> inMemoryDatabase "Interage"
                user -> salesController "Interage com"
            }

            swaggerContainer = container "Swagger UI" "Interface para documentação da API." "Swagger" {
                apiContainer -> this "Fornece documentação"
            }
            
            database = container "InMemoryDatabase" "Banco de dados em memória para armazenar as vendas (Armazena no arquivo Sales.json)." "In-Memory Database" {
                apiContainer -> this "Persiste dados"
                
                sale = component "Sale" "Representa uma sale de produtos." "C#"
                vendedor = component "Vendedor" "Representa um vendedor." "C#"
                item = component "Item" "Representa um item vendido." "C#"
                statusSale = component "StatusSale" "Representa o status de uma sale." "C#"
                
                sale -> vendedor "Contém" "1..*"
                sale -> item "Possui" "*..1"
                sale -> statusSale "Tem" "1"
            }
        }
    }

    views {
        systemContext softwareSystem {
            include *
            autolayout lr
        }

        container softwareSystem {
            include *
            autolayout lr
        }

        component apiContainer {
            include *
            autolayout lr
        }

        component database {
            include *
            autolayout lr
        }

        theme default
    }

}
