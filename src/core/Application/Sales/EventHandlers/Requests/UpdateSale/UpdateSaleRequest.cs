using System.Text.Json.Serialization;
using Domain.Enums;
using MediatR;

namespace Application.Sales.EventHandlers.Requests.UpdateSale;

/// <summary>
/// Request de atualização do status da venda
/// </summary>
public class UpdateSaleRequest : IRequest<Guid?>
{
    [JsonIgnore]
    public Guid Id { get; set; }

    /// <summary>
    /// Status da venda a ser atualizada
    /// <example>PaymentApproved</example>
    /// </summary>
    public StatusSaleEnum Status { get; set; }
}