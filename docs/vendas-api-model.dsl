workspace {

    model {
        user = person "User" "Pessoa que utiliza a API para gerenciar vendas."

        softwareSystem = softwareSystem "Sistema de Vendas API" "API para gerenciar vendas de produtos." {
            apiContainer = container "API REST (.NET Core)" "Exponha opera��es REST para gerenciar vendas." "C# .NET Core" {
                user -> this "Utiliza"
            }
            swaggerContainer = container "Swagger UI" "Interface para documenta��o da API." "Swagger" {
                apiContainer -> this "Fornece documenta��o"
            }
            database = container "InMemoryDatabase" "Banco de dados em mem�ria para armazenar vendas." "In-Memory Database" {
                apiContainer -> this "Persiste dados"
            }
        }
    }

    views {
        systemContext softwareSystem {
            include *
            autolayout lr
        }

        container softwareSystem {
            include *
            autolayout lr
        }

        component apiContainer {
            vendasController = component "VendasController" "Controlador para opera��es de vendas." "C#" {
                user -> this "Interage com"
            }
            vendasService = component "VendasService" "Servi�o para l�gica de neg�cios de vendas." "C#" {
                vendasController -> this "Chama m�todos"
            }
            vendaRepository = component "VendaRepository" "Reposit�rio para persist�ncia de vendas." "C#" {
                vendasService -> this "Chama m�todos"
            }
            inMemoryDatabase = component "InMemoryDatabase" "Banco de dados em mem�ria para armazenar vendas." "In-Memory Database" {
                vendaRepository -> this "Interage"
            }
        }

        theme default
    }

}
