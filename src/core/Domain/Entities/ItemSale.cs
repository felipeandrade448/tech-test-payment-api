﻿namespace Domain.Entities;

public class ItemSale : AuditableEntity, IAggregateRoot
{
    public string? Name { get; set; }
    public decimal? Price { get; set; }
    public int? Quantity { get; set; }
}