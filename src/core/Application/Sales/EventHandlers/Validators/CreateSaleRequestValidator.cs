using Application.Common.Validation;
using Application.Sales.EventHandlers.Requests.CreateSale;
using FluentValidation;
using Shared.Exceptions;

namespace Application.Sales.EventHandlers.Validators;

public class CreateSaleRequestValidator : CustomValidator<CreateSaleRequest>
{
    public CreateSaleRequestValidator()
    {
        RuleFor(sale => sale.Date)
            .Cascade(CascadeMode.Stop)
            .NotNull()
            .WithMessage(ErrorsMessages.Validate.ValidateSale.DateRequired);

        RuleFor(sale => sale.Seller)
            .NotNull()
            .WithMessage(ErrorsMessages.Validate.ValidateSale.SellerRequired)
            .SetValidator(new CreateSellerToSaleRequestValidator());

        RuleFor(sale => sale.Items)
            .NotNull()
            .WithMessage(ErrorsMessages.Validate.ValidateSale.MinItemsRequired)
            .NotEmpty()
            .WithMessage(ErrorsMessages.Validate.ValidateSale.MinItemsRequired);

        RuleForEach(sale => sale.Items)
            .SetValidator(new CreateItemToSaleRequestValidator());
    }
}