using Domain.Enums;

namespace Application.Sales.EventHandlers.Dtos;

public record SaleResponse(Guid Id, string Code, DateTime Date, StatusSaleEnum Status, SellerResponse Seller, List<ItemSaleResponse>? Items);
public record SellerResponse(Guid Id, string Document, string Name, string Email, string Phone);
public record ItemSaleResponse(string Name, decimal Price, int Quantity);
