## INSTRUÇÕES PARA O TESTE TÉCNICO

- Crie um fork deste projeto (https://gitlab.com/Pottencial/tech-test-payment-api/-/forks/new). É preciso estar logado na sua conta Gitlab;
- Adicione @Pottencial (Pottencial Seguradora) como membro do seu fork. Você pode fazer isto em  https://gitlab.com/`your-user`/tech-test-payment-api/settings/members;
 - Quando você começar, faça um commit vazio com a mensagem "Iniciando o teste de tecnologia" e quando terminar, faça o commit com uma mensagem "Finalizado o teste de tecnologia";
 - Commit após cada ciclo de refatoração pelo menos;
 - Não use branches;
 - Você deve prover evidências suficientes de que sua solução está completa indicando, no mínimo, que ela funciona;

## O TESTE
- Construir uma API REST utilizando .Net Core, Java ou NodeJs (com Typescript);
- A API deve expor uma rota com documentação swagger (http://.../api-docs).
- A API deve possuir 3 operações:
  1) Registrar sale: Recebe os dados do vendedor + itens vendidos. Registra sale com status "Aguardando pagamento";
  2) Buscar sale: Busca pelo Id da sale;
  3) Atualizar sale: Permite que seja atualizado o status da sale.
     * OBS.: Possíveis status: `Pagamento aprovado` | `Enviado para transportadora` | `Entregue` | `Cancelada`.
- Uma sale contém informação sobre o vendedor que a efetivou, data, identificador do pedido e os itens que foram vendidos;
- O vendedor deve possuir id, cpf, nome, e-mail e telefone;
- A inclusão de uma sale deve possuir pelo menos 1 item;
- A atualização de status deve permitir somente as seguintes transições: 
  - De: `Aguardando pagamento` Para: `Pagamento Aprovado`
  - De: `Aguardando pagamento` Para: `Cancelada`
  - De: `Pagamento Aprovado` Para: `Enviado para Transportadora`
  - De: `Pagamento Aprovado` Para: `Cancelada`
  - De: `Enviado para Transportador`. Para: `Entregue`
- A API não precisa ter mecanismos de autenticação/autorização;
- A aplicação não precisa implementar os mecanismos de persistência em um banco de dados, eles podem ser persistidos "em memória".

## PONTOS QUE SERÃO AVALIADOS
- Arquitetura da aplicação - embora não existam muitos requisitos de negócio, iremos avaliar como o projeto foi estruturada, bem como camadas e suas responsabilidades;
- Programação orientada a objetos;
- Boas práticas e princípios como SOLID, DDD (opcional), DRY, KISS;
- Testes unitários;
- Uso correto do padrão REST;

# Sistema de Sales - API
> API para gerenciar sales de produtos.

Todos os diagramas foram gerados pelo arquivo `sales-api-model.dsl` e compilados na plataforma https://structurizr.com/dsl

## Run project

```
Instalar as dependências/libs
dotnet restore ./src/services/SaleService/SaleService.csproj

Buildar o projeto
dotnet build ./src/services/SaleService/SaleService.csproj

Rodar o projeto
dotnet run --project ./src/services/SaleService/SaleService.csproj
```

## Diagrama de container

![[Container] Sistema de Sales API](docs/assets/img/container-api.png)

## Diagrama de componentes

![[Componentes] Sistema de Sales API - API REST](docs/assets/img/restapi-component.png)

## Diagrama de relacionamento

### Definição das entidades
![[DER] Diagrama de entidade e relacionamento](docs/assets/img/class-diagram.png)

![[Componentes] Diagrama de entidade e relacionamento](docs/assets/img/relacionamento-component.png)

## Testes unitários e de integração

O resultado do coverage pode ser acessado em `docs/coverage.json` ou `docs/coverage.html`

![[Coverage] Resultado do coverage do código](docs/assets/img/coverage.png)

## CI/CD

O deploy está configurado para ser orquestrado via github e serverless, sendo criada uma lambda (AWS) para acesso ao conteúdo da API

Para acesso a API e sua documentação [clique aqui](https://2s7w8wyei6.execute-api.us-east-1.amazonaws.com/dev/swagger/index.html)

## Evidências
API rodando
![alt text](docs/assets/img/image.png)

Inserindo uma venda
![alt text](docs/assets/img/image-1.png)
![alt text](docs/assets/img/image-2.png)

Obtendo uma venda
![alt text](docs/assets/img/image-3.png)

Atualizando uma venda
![alt text](docs/assets/img/image-4.png)
![alt text](docs/assets/img/image-5.png)

Atualizando uma venda para um status fora da regra de negócio
![alt text](docs/assets/img/image-6.png)

## Sugestões de melhorias

Considerando a usabilidade do usuário/sistema seria interessante incluir endpoints de busca em lote, podento filtrar por vendedor e data.

O ideal é termos um cadastro separado para os vendedores, sabendo que o mesmo poderá realizar mais de uma venda. Considerando que a API seja para realizar uma integração entre sistemas, podemos considerar que os dados sempre serão enviados corretamente.

Utilizei a estrutura de objetos para salvar em um arquivo .json, pois, por ser uma API que atende a um caso específico (Gestão de vendas), minha sugestão seria mantermos os dados em um banco de dados não relacional (NoSQL). Com isso, teríamos que ter o cuidado em atualizações dos dados do vendedor.

Como incluí o deploy na AWS Lambda, onde o `Runtime` da AWS Lambda permite somente `dotnet6`, o projeto foi desenvolvido usando o `SDK 6.0` do .NET. Uma futura melhoria seria modificar o ambiente para `K8S`, onde, em uma eventual escala em microserviços, facilitaria a gestão de recursos. 