﻿namespace Shared.Exceptions;

public static class ErrorsMessages
{
    public const string InternalServerError = "Ocorreu um erro inesperado. Tente novamente em alguns mintos ou contate o administrador do sistema.";
    public const string NotFoundTable = $"Não foi encontrada a tabela solicitada.";

    public static class Sale
    {
        public const string NotFound = "Venda não encontrada.";
        public const string UnableToUpdateSaleStatus = "Não é possível atualizar o status da venda de {0} para {1}.";
    }

    public static class Validate
    {
        public const string BadRequest = "Requisição incorreta.";

        public static class ValidateSale
        {
            public const string DateRequired = "A data é de preenchimento obrigatório.";
            public const string SellerRequired = "O vendedor é de preenchimento obrigatório.";
            public const string MinItemsRequired = "É necessário preencher no mínimo 1 item de venda.";


            public static class ValidateItem
            {
                public const string NameRequired = "O nome do item é de preenchimento obrigatório.";
                public const string PriceRequired = "O preço é de preenchimento obrigatório.";
                public const string QuantityRequired = "A quantidade é de preenchimento obrigatório.";
            }
        }

        public static class ValidateSeller
        {
            public const string NameRequired = "O nome do vendedor é de preenchimento obrigatório.";
            public const string DocumentRequired = "O documento do vendedor é de preenchimento obrigatório.";
            public const string EmailRequired = "O email do vendedor é de preenchimento obrigatório.";
            public const string EmailInvalid = "O email do vendedor deve ser válido.";
            public const string PhoneRequired = "O telefone do vendedor é de preenchimento obrigatório.";
        }

        public static class ValidateDocument
        {
            public const string InvalidDocument = "O documento do vendedor deve ser válido.";
        }
    }

}