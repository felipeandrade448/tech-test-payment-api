using Application.Common.Validation;
using Application.Sales.EventHandlers.Requests.CreateSale;
using FluentValidation;
using Shared.Exceptions;

namespace Application.Sales.EventHandlers.Validators;

public class CreateSellerToSaleRequestValidator : CustomValidator<CreateSellerToSaleRequest>
{
    public CreateSellerToSaleRequestValidator()
    {
        RuleFor(sale => sale.Name)
            .Cascade(CascadeMode.Stop)
            .NotNull()
            .WithMessage(ErrorsMessages.Validate.ValidateSeller.NameRequired)
            .NotEmpty()
            .WithMessage(ErrorsMessages.Validate.ValidateSeller.NameRequired);

        RuleFor(sale => sale.Document)
            .Cascade(CascadeMode.Stop)
            .NotNull()
            .WithMessage(ErrorsMessages.Validate.ValidateSeller.DocumentRequired)
            .NotEmpty()
            .WithMessage(ErrorsMessages.Validate.ValidateSeller.DocumentRequired)
            .Must(document => document?.Length is ValidatorsConstants.CpfLength or ValidatorsConstants.CnpjLength)
            .WithMessage(ErrorsMessages.Validate.ValidateDocument.InvalidDocument)
            .Must((documentNumber, document) =>
            {
                if (documentNumber.Document?.Length == ValidatorsConstants.CpfLength)
                    return CpfCnpjValidator.IsValidCpf(document);
                if (documentNumber.Document?.Length == ValidatorsConstants.CnpjLength)
                    return CpfCnpjValidator.IsValidCnpj(document);
                return false;
            })
            .WithMessage(ErrorsMessages.Validate.ValidateDocument.InvalidDocument);

        RuleFor(sale => sale.Email)
            .Cascade(CascadeMode.Stop)
            .NotNull()
            .WithMessage(ErrorsMessages.Validate.ValidateSeller.EmailRequired)
            .NotEmpty()
            .WithMessage(ErrorsMessages.Validate.ValidateSeller.EmailRequired)
            .EmailAddress()
            .WithMessage(ErrorsMessages.Validate.ValidateSeller.EmailInvalid);

        RuleFor(sale => sale.Phone)
            .Cascade(CascadeMode.Stop)
            .NotNull()
            .WithMessage(ErrorsMessages.Validate.ValidateSeller.PhoneRequired)
            .NotEmpty()
            .WithMessage(ErrorsMessages.Validate.ValidateSeller.PhoneRequired);
    }
}